const mongoClient = require('mongodb').MongoClient;

let _db;

const atlasUri = 'mongodb+srv://powe2009:roseros9@cluster0-jb7dw.mongodb.net/admin';
const localUri = 'mongodb://localhost:27017';

const mongoConnect = cb => {
  mongoClient.connect(
    localUri,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true
      }
    )
    .then(client => {
      _db = client.db('shop');
      console.log('\n\n Connected');
      
      cb(_db);
    })
    .catch(err => {
      console.log(err);
    });
};

const getDb = () => {
  if (_db) {
    return _db;
  }
  throw new Error('No db connection');
}

exports.mongoConnect = mongoConnect;
exports.getDb = getDb;

