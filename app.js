const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const User = require('./models/user');

const defaultUserId = '5d9fbf43bf6f3621b4dbe3c5';
const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');
const errorController = require('./controllers/error');

const localUri = 'mongodb://localhost:27017/node-mongoose';

const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use( (req, res, next) => {
  User.findById(defaultUserId )
    .then(user => {
      if (user) {
        req.user = user;
        next();
      } else {
        throw new Error("User by this id not found: ", defaultUserId);
      }
    })
    .catch(error => {
      console.log("TCL: error", error)
    })
})

app.use('/admin', adminRoutes);
app.use(shopRoutes);

app.use(errorController.get404);

mongoose.connect( localUri, {useNewUrlParser: true,  useUnifiedTopology: true})
  .then(conn => {
    app.listen(3000);
  })
  .catch(error => {
    console.log("TCL: error", error)
  })
    